Prueba técnica ThePowerMBA - Parte 1
===

Para la realización de esta parte, se asumieron los siguientes escenarios:

1. Se consideró que el Pokémon inicial o raíz no es una evolución en si misma. Por tanto todos los que tengan al menos una evolución anterior son en sí mismos evoluciones de otro Pokémon más primitivo.
2. De manera análoga, en la segunda pregunta he asumido que para ser una primera evolución, el pokémon de primera evolución es el inmediatamente seguido de evolucionar el pokémon raíz

### Consultas aplicadas

- Pregunta 1
```javascript
db.samples_pokemon.find({'prev_evolution': {$exists: true} }, { $where: 'this.prev_evolution.length >= 1', _id: 0, 'name': 1, 'num': 1, 'spawn_time': 1 }).limit(0)
```

- Pregunta 2
```javascript
db.samples_pokemon.find({ 'prev_evolution.0': {  $exists: true }, 'prev_evolution.1': {  $exists: false }, $and: [ { 'avg_spawns': { $gt: 4 } }] }, { _id: 0, 'name': 1, 'num': 1}).limit(0)
```

Los resultados obtenidos de sendas preguntas, se encuentran el en el directorio `output` como `pokemons1.json` y `pokemons2.json` respectivamente.
